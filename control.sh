#!/bin/ksh


set_in_order() {

NUM=0

clear

for i in `cat get_names` ; do

        if [[ ${i} != [[:blank:]#]* ]] ; then
          NUM=`expr ${NUM} + 1`
          [[ -n ${i} ]] && echo ${i##*.} | tr '\n' ',' >> not_final_names
        fi

done < get_names

  cat not_final_names | rev | cut -c 2- | rev >> final_names

rm -f get_names not_final_names

}

echo -e "\n"

printf %b "Coloca aqui los jobs, da igual como y pulsa Control+d cuando termines:\n"

touch get_names

xargs -0 > get_names

set_in_order get_names

echo -e "\nHas introducido ${NUM} jobs o cadenas\n"

echo -e "Aqui tienes los jobs:\n\n`cat final_names`\n"

rm final_names