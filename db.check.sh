#!/bin/bash

#################################################
#       Check DB´s on sbo and sks servers       #
#################################################

#set -x


#################################################
#                Global variables               #
#################################################

LOG_FILE="/opt/log/db_check_log_`date '+%F'`_`date '+%k-%M-%S'`"
DATABASES="${2}"
SERVER_TYPE="`echo ${1} | cut -d. -f1`"
CONT="0"
HELP="This script need´s the domain name as the first argument, and the databases as the second. Expl: ./db_check.sh name.domain \"dbonedb dbtwodb dbthreedb\""


#################################################
#                      main                     #
#################################################

if [[ -z ${1} ]] || [[ -z ${2} ]] ; then

   echo -e ${HELP}
   exit 1

fi


if [[ ! -d /tmp/db_check ]] ; then

   mkdir /tmp/db_check

fi

read -n 1 -s -r -p "Confirm that you want to check databases for ${1}. Press any key to continue: "

echo ""

echo -e "Checking for the server ${1}" >> $LOG_FILE


#This loop is what does the real work, just check every database using net_reload.sh
# $db_name is the name of the database and $target_name is the name that it will be


for db_name in ${DATABASES} ; do

    target_name=${db_name}

       if [[ ${db_name} == "eventdb" ]] ; then

            target_name=${db_name}_${SERVER_TYPE}

       fi

    echo -e "\n##############################################################################################\n" | tee -a $LOG_FILE

    echo -e "\n########################################## `date +%k:%M:%S` ##########################################\n" | tee -a $LOG_FILE

    echo -e "\n##############################################################################################\n" | tee -a $LOG_FILE

    net_reload.sh -src=${db_name} -tgt=${target_name}  -user=lm -vnode=${1} -host=${1}.corp.leroymerlin.com -bg -verbose | tee -a $LOG_FILE

    CONT=`expr $CONT + 1`

done

echo -e "\n##############################################################################################\n"

echo -e "They have been checked ${CONT} databases" >> ${LOG_FILE}

echo -e "They have been checked ${CONT} databases, please check the log file in ${LOG_FILE}"

echo -e "\n##############################################################################################\n"
