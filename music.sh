#!/bin/bash

#######################################################################################################################
# This script allow you to; play, pause, stop, next, previous your VLC instance without move your eyes from terminal. # 
#######################################################################################################################


##########################
#   GLOBAL VARIABLES	 #
##########################


HELP="\nYou must to pass a parameter when you call the script. Parameters are; play, pause, stop, next and prev (previous)\n"


##########################
#        FUNCTIONS       #
##########################


check_param() {

if [[ -z ${1} ]] || [[ ${1} = "help" ]] ; then
  
  echo -e ${HELP}
  exit 1

else

  case ${1} in

	play)
	    INSTRUCTION=Play
	    ;;
	pause)
	    INSTRUCTION=Pause
	    ;;
	stop)
	    INSTRUCTION=Stop
	    ;;
	next)
	    INSTRUCTION=Next
	    ;;
	prev)
	    INSTRUCTION=Previous
	    ;;
	*)
	    echo -e "${HELP}"
	    exit 1
	    ;;

  esac

fi

}


##########################
#          MAIN          #
##########################


check_param ${1} ${HELP}

if [[ -n ${2} ]] && [[ ${2} =~ [[:digit:]] ]] && [[ ${INSTRUCTION} =~ (Next|Previous) ]] ; then

  CONT="${2}"
	
    for (( i=1 ; i<=${CONT} ; i++ )) ; do

      sleep 0.1 # Change this value if VLC can´t handle the "dbus-send" instruction

      dbus-send --type=method_call --dest=org.mpris.MediaPlayer2.vlc /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.${INSTRUCTION}

    done

else 

  dbus-send --type=method_call --dest=org.mpris.MediaPlayer2.vlc /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.${INSTRUCTION}

fi